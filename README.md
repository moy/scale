# Scale

Semi-automatic grading for lab works. Put grading annotations in the source file, and the script computes the sum.

## Documentation

```
./scale -h
```

## Features

* Compute the sum of points for all questions.

* Tells you if you forgot to grade a question.

* Customizable through command-line or configuration file (`--spec`)

## Example

See [example.c](example.c).

```
$ ./scale --spec spec.py --scale                      
// BAREME (commentaires): ?/1
// BAREME (tests): ?/1
// BAREME (exo1): ?/3
// BAREME (exo2): ?/3.5
// BAREME (exo3): ?/5
// BAREME (exo4): ?/3.5
$ grep BAREME example.c 
// BAREME (commentaires): 0,5/1
// BAREME (tests): 0,5/1
    // BAREME (exo1): 1/3
    // BAREME (exo2): 3.5/3.5
    // BAREME (exo3): 5/5
    // BAREME (exo4): 3.5/3.5
$ ./scale --spec spec.py example.c 
0.5 / 1.0 (commentaires)
0.5 / 1.0 (tests)
1.0 / 3.0 (exo1)
3.5 / 3.5 (exo2)
5.0 / 5.0 (exo3)
3.5 / 3.5 (exo4)
--------
14.0 / 17.0
```
