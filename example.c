#include <stdio.h>

// BAREME (commentaires): 0,5/1
// BAREME (tests): 0,5/1
int  main(void) {
    // BAREME (exo1): 1/3
    int x;

    // BAREME (exo2): 3.5/3.5
    x = x + 1;

    // BAREME (exo3): 5/5
    printf("%d\n", x);

    // BAREME (exo4): 3.5/3.5
    return 0;
}
